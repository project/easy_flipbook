<?php
//https://www.drupal.org/docs/8/creating-custom-modules/creating-custom-blocks/create-a-custom-block


namespace Drupal\easy_flipbook\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a 'EASY FlipBook Free' Block.
 *
 * @Block(
 *   id = "easy_flipbook",
 *   admin_label = @Translation("EASY FlipBook Free block"),
 *   category = @Translation("EASY FlipBook Free"),
 * )
 */
class EASYFlipBookBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (!empty($config['easyflipbook_block_settings_src_type'])) {
      $src_type = $config['easyflipbook_block_settings_src_type'];
    }
    else {
      $src_type = "pdf";
    }

    if (!empty($config['easyflipbook_block_settings_src_path'])) {
      $src_path = $config['easyflipbook_block_settings_src_path'];
    }
    else {
      $src_path = "";
    }

    $flipbook_src_options='';

    if ($src_type=="pdf"){ 
      $flipbook_src_options = "pdf: '".$src_path."',";
    }

    if ($src_type=="images"){
        $fi = glob(getcwd().$src_path."/*.*");

        $tt='';
        foreach (glob("*.txt") as $filename) {
          $tt=$tt. "$filename size " . filesize($filename) . "\n";
      }

      $flipbook_src_options =     
    "pageCallback: function(n) {".
          "return {".
            "type: 'image',".
            "src: '".$src_path."/'+(n+1)+'.jpg',".
            "interactive: false".
          "};".
        "},".
        "pages: ".count($fi);
    }
        

    return [
      '#theme' => 'my_template',
      '#test_var' => $tt."::".getcwd(),
      '#val1' => getcwd().$src_path."/*.*".':'.count($fi),
      '#src_type' => $this->t($src_type),
      '#src_path' => $this->t($src_path),
      '#flipbook_src_options' => $this->t($flipbook_src_options),
      '#attached' => [
        'library' => [
          'easy_flipbook/easy_flipbook',
        ],
      ],
    ];
  }


  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }
 
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
 
    $options = array(
      'pdf' => t('pdf'),
      'images' => t('images')
    );

    $form['easyflipbook_block_settings_src_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Source type'),
      '#options' => $options,
      '#description' => $this->t('Type source for your presentation.'),
      '#default_value' => !empty($config['easyflipbook_block_settings_src_type']) ? $options[$config['easyflipbook_block_settings_src_type']] : 'images',
    ];

    $form['easyflipbook_block_settings_src_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path'),
      '#description' => $this->t('Path to resource. For example: /modules/easy_flipbook_2.1_free/fb_sources/pdfs/RUS.pdf'),
      '#default_value' => !empty($config['easyflipbook_block_settings_src_path']) ? $config['easyflipbook_block_settings_src_path'] : '/modules/easy_flipbook_2.1_free/fb_sources/images',
    ];
 
    return $form;
  }
 
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configuration['easyflipbook_block_settings_src_type'] = $values['easyflipbook_block_settings_src_type'];
    $this->configuration['easyflipbook_block_settings_src_path'] = $values['easyflipbook_block_settings_src_path'];
  }
}